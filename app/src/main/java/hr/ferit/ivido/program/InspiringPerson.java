package hr.ferit.ivido.program;




public class InspiringPerson {

    public String getDateBorn() {
        return dateBorn;
    }
    public String getDateDied() {
        return dateDied;
    }

    public String getName() {
        return name;
    }


    private String dateBorn;
    private String dateDied;
    private String name;

    InspiringPerson(String name, String dateBorn, String dateDied) {
        this.dateBorn=dateBorn;
        this.dateDied=dateDied;
        this.name=name;

    }

}
