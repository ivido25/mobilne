package hr.ferit.ivido.program;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    TextView txtDennisRitchie, txtAlanTuring, txtLinusTorvalds;
    ImageView imgDennisRitchie, imgAlanTuring, imgLinusTorvalds;
    public String descriptA="", descriptD="", descriptL="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InspiringPerson dennis = new InspiringPerson("Dennis Ritchie","9.9.1941.","12.10.2012." );
        InspiringPerson alan = new InspiringPerson("Alan Turing","23.6.1912.","7.6.1954.");
        InspiringPerson linus = new InspiringPerson("Linus Torvalds","28.12.1969.","...");
        descriptA = alan.getName()+" ("+alan.getDateBorn()+"-"+alan.getDateDied()+")\n";
        descriptD = dennis.getName()+" ("+dennis.getDateBorn()+"-"+dennis.getDateDied()+")\n";
        descriptL = linus.getName()+" ("+linus.getDateBorn()+"-"+linus.getDateDied()+")\n";
        initUI();
    }

    private void initUI() {
        this.imgAlanTuring = (ImageView) findViewById(R.id.imgAlanTuring);
        this.imgDennisRitchie = (ImageView) findViewById(R.id.imgDennisRitchie);
        this.imgLinusTorvalds = (ImageView) findViewById(R.id.imgLinusTorvalds);

        this.txtDennisRitchie = (TextView) findViewById(R.id.txtDennisRitchie);
        this.txtLinusTorvalds = (TextView) findViewById(R.id.txtLinusTorvalds);
        this.txtAlanTuring = (TextView) findViewById(R.id.txtAlanTuring);

        descriptA+=getString(R.string.txtAlanTuring);
        this.txtAlanTuring.setText(descriptA);
        descriptD+=getString(R.string.txtDennisRitchie);
        this.txtDennisRitchie.setText(descriptD);
        descriptL+=getString(R.string.txtLinusTorvalds);
        this.txtLinusTorvalds.setText(descriptL);

        this.imgAlanTuring.setOnClickListener(this);
        this.imgDennisRitchie.setOnClickListener(this);
        this.imgLinusTorvalds.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case (R.id.imgAlanTuring):
                Toast.makeText(this, R.string.toastTuring, Toast.LENGTH_LONG).show();
                break;
            case (R.id.imgDennisRitchie):
                Toast.makeText(this, R.string.toastRitchie, Toast.LENGTH_LONG).show();
                break;
            case (R.id.imgLinusTorvalds):
                Toast.makeText(this, R.string.toastLinus, Toast.LENGTH_LONG).show();
                break;
        }
    }
}